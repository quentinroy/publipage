from flask import Flask
from publipage import pub_blueprint

app = Flask(__name__)
# register the publications blueprint
app.register_blueprint(pub_blueprint)
# fix urls in frozen flask
app.config['FREEZER_RELATIVE_URLS'] = True

if __name__ == "__main__":
    app.run(debug=True)
