Simple publication page.


How To Install
==============

.. sourcecode:: bash

    virtualenv venv  # create a virtual environment for python
    source venv/bin/activate  # link python to venv's binaries
    pip install -r requirements.txt  # install dependencies


How to serve
============

.. sourcecode:: bash

    python serve.py


How to create a static version
==============================

.. sourcecode:: bash

    python freeze.py

This will create an independant static version of the page under the build directory.


How to organize
===============

By default, the bibliography is imported from the biblio folder (you can change it in
settings.py).

Directories containing subdirectories will be used as category of publications (currently unused).
Directories without subfolders will be used as publication entry.

Inside publications directory:

* bibtex are automatically parsed to retrieve publication information.
* jpeg or png files are used as publication icon.
* pdf files are used as publication paper.
* rst files are used for publication description.

Inside categories directory:

* rst files are used for category description (category are currently unused)


TODO
====

* Display category.