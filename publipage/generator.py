# -*- coding: utf-8 -*-

from jinja2 import Template
import bib
import os
import distutils.core
from encoding import filepath_to_uri

_module_name = os.path.dirname(__file__)


def _make_biblio_files_urls(biblio_rec, target_dir):
    if 'files' in biblio_rec:
        if not 'urls' in biblio_rec:
            biblio_rec['urls'] = {}
        urls = biblio_rec['urls']
        for file_type, files in biblio_rec['files'].iteritems():
            if not file_type.startswith('_'):
                if not file_type in urls:
                    urls[file_type] = []
                for file_path in files:
                    target_path = os.path.join(target_dir, file_path)
                    dirname = os.path.dirname(target_path)
                    if not os.path.exists(dirname):
                        os.makedirs(dirname)
                    distutils.file_util.copy_file(file_path, target_path)
                    urls[file_type].append(filepath_to_uri(target_path))

    if 'subdirectories' in biblio_rec:
        for subdir in biblio_rec['subdirectories']:
            _make_biblio_files_urls(subdir, target_dir)


def generate(biblio_path='biblio',
             target_path='build',
             static_sub_path='.',
             publipage_name='index.html',
             template_path=os.path.join(_module_name, 'templates/publis.html'),
             static_path=os.path.join(_module_name, 'static'),
             biblio_files_sub_path='files'):

    # compile the template
    publi_template = Template(open(template_path, 'r').read())

    # Load the bibliography
    biblio = bib.load(os.path.join(biblio_path))

    # render the biblio page
    publipage_content = publi_template.render(
        categories=biblio['subdirectories'])

    # create the build directory if needed
    if not os.path.exists(target_path):
        os.makedirs(target_path)

    # copy static files
    target_static_path = os.path.join(target_path, static_sub_path)
    if os.path.exists(static_path):
        if not os.path.exists(target_static_path):
            os.makedirs(target_static_path)
        distutils.dir_util.copy_tree(static_path, target_static_path)

    # copy biblio files
    _make_biblio_files_urls(biblio,
                            os.path.join(target_path, biblio_files_sub_path))

    # write the publi page
    publipage_path = os.path.join(target_path, publipage_name)
    with open(publipage_path, 'w') as publipage_file:
        publipage_file.write(
            publipage_content.encode('ascii', 'xmlcharrefreplace'))


if __name__ == '__main__':
    generate()
