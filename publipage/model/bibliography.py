from collections import OrderedDict
from publipage.model.authorsregister import AuthorsRegister


class Bibliography(object):
    def __init__(self, name=None, description=None, path=None):
        self.name = name
        self.description = description
        self._publications = OrderedDict()
        self._categories = OrderedDict()
        self._cat_view = self._categories.viewvalues()
        self._pub_view = self._publications.viewvalues()
        self.path = path
        self._authors_register = AuthorsRegister()

    def add_publication(self, publication, safe_id=False):
        # check if the publication is already in a bibliography
        if publication.bibliography:
            raise ValueError("This publication is already registered "
                             "in a bibliography")
        # manage id in unicity
        if safe_id:
            publication.set_id(self.get_free_pub_id(publication.id))
        elif self.has_publication(publication.id):
            raise ValueError("A publication with the same id is already"
                             " registered.")
        # add the publication
        self._publications[publication.id] = publication
        publication._bibliography = self

    def add_category(self, category, safe_id=False):
        if category.bibliography:
            raise ValueError("A category cannot be registered in"
                             " two bibliographies.")
        if safe_id:
            category.set_id(self.get_free_cat_id(category.id))
        elif self.has_category(category.id):
            raise ValueError("A category with the same id is "
                             "already registered")
        # add the category
        self._categories[category.id] = category
        category._bibliography = self

    def get_free_pub_id(self, original_id):
        return self._get_free_id(original_id, self._publications)

    def get_free_cat_id(self, original_id):
        return self._get_free_id(original_id, self._categories)

    @staticmethod
    def _get_free_id(original_id, existing_ids):
        if not original_id in existing_ids:
            return original_id
        i = 2
        pattern = original_id + "_{}"
        proposition = pattern.format(i)
        while proposition in existing_ids:
            i += 1
            proposition = pattern.format(i)
        return proposition

    @property
    def categories(self):
        return self._cat_view

    @property
    def publications(self):
        return self._pub_view

    @property
    def authors_register(self):
        return self._authors_register

    def has_category(self, cat_id):
        return cat_id in self._categories

    def has_publication(self, pub_id):
        return pub_id in self._publications

    def iter_main_categories(self):
        for category in self.categories:
            if not category.parent:
                yield category

    def get_category(self, cat_id):
        return self._categories[cat_id]

    def get_publication(self, pub_id):
        return self._publications[pub_id]
