from author import Author
import unicodedata


class AuthorsRegister(object):

    class AuthorAlreadyRegistered(ValueError):
        pass

    def __init__(self):
        self._authors = dict()

    def get(self, author):
        if not isinstance(author, Author):
            author = Author.parse(author)
        author_hash = self._author_hash(author)
        return self._authors.get(author_hash)

    def add(self, author):
        if not isinstance(author, Author):
            author = Author.parse(author)
        author_hash = self._author_hash(author)
        if author_hash in self._authors:
            raise self.AuthorAlreadyRegistered('Author already registered: ' +
                                               author)
        else:
            self._authors[author_hash] = author
        return author

    def __contains__(self, author):
        if isinstance(author, Author):
            author = self._author_hash(author)
        return author in self._authors

    @staticmethod
    def _author_hash(author):
        author_hash = author.first_name + author.last_name
        author_hash = author_hash.lower()
        # remove accents and special characters
        author_hash = unicodedata.normalize('NFKD', author_hash)
        author_hash = author_hash.encode('ASCII', 'ignore')
        return author_hash
