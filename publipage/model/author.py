from nameparser import HumanName


class Author(object):
    def __init__(self, first_name, last_name,
                 middle_name=None, email_adress=None, website=None):
        self.first_name = first_name
        self.middle_name = middle_name
        self.last_name = last_name
        self.email_address = email_adress
        self.webste = website

    def __str__(self):
        return self.full_names()

    @staticmethod
    def abbrev_name(name):
        return name[0].upper() + '.'

    def full_names(self):
        fname = ''
        if self.first_name:
            fname += self.first_name + ' '
        return fname + self.last_name

    def abbrev_names(self):
        fname = ''
        if self.first_name:
            fname += self._abbrev_name(self.first_name) + ' '
        return fname + self.last_name

    def __repr__(self):
        return "{} ({}, {})".format(self.__class__,
                                    self.last_name,
                                    self.first_name)

    @classmethod
    def parse(cls, nameStr):
        hname = HumanName(nameStr)
        return Author(hname.first, hname.last, hname.middle)
