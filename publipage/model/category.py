from collections import OrderedDict

class Category(object):
    def __init__(self,
                 cat_id,
                 bibliography=None,
                 name=None,
                 description=None,
                 safe_id=False):
        self.publications = OrderedDict()
        self._parent = None
        self._id = cat_id
        self.name = name
        self.description = description
        self._bibliography = None
        if bibliography:
            bibliography.add_category(self, safe_id)

    @property
    def parent(self):
        return self._parent

    @property
    def bibliography(self):
        return self._bibliography

    @property
    def id(self):
        return self._id

    def set_id(self, value):
        if value != self.id:
            if self.bibliography:
                if self.bibliography.has_category(value):
                    raise ValueError('This id is already taken in the '
                                     'bibliography.')
                del self.bibliography._categories[self.id]
                self.bibliography._categories[value] = self
            self._id = value

    def set_parent(self, cat_id):
        category = self.bibliography.get_category(cat_id)
        self._parent = category

    def iter_publications(self):
        if self.bibliography:
            for publication in self.bibliography.publications:
                if publication.category is self:
                    yield publication
        else:
            raise ValueError("The category is not associated with "
                             " a bibliography")

    def add_publication(self, publication, safe_id=False):
        if not self.bibliography:
            raise 'This category is not associated with a bibliography.'
        # case id
        if not hasattr(publication, 'id'):
            publication = self.bibliography.get_publication(publication)
        # case the publication is not yet the bibliography
        if not self.bibliography.has_publication(publication.id):
            self.bibliography.add_publication(publication)
        # case the publication is already in the bibliography
        elif self.bibliography.get_publication(publication.id) \
                is not publication:
            if not safe_id:
                raise ValueError('The bibliography already contains a '
                                 'publication using this id.')
            else:
                self.bibliography.add_publication(publication)
        publication.set_category(self.id)

    def has_parent(self, cat_id):
        if not self.parent:
            return False
        elif self.parent.id == cat_id:
            return True
        else:
            return self.parent.has_parent(cat_id)

    def level(self):
        if not self.parent:
            return 0
        else:
            return self.parent.level() + 1

    def iter_subcategories(self):
        if not self.bibliography:
            raise ValueError("The category is not associated"
                             " with a bibliography")
        for category in self.bibliography.categories:
            if category.parent is self:
                yield category