from author import Author
from publipage.model.authorsregister import AuthorsRegister
from bibliography import Bibliography
from category import Category
from publication import Publication