
class Publication(object):

    def __init__(self,
                 pub_id,
                 title,
                 bibliography=None,
                 authors=None,
                 book=None,
                 description=None,
                 safe_id=False,
                 image=None,
                 pdf=None,
                 year=None,
                 description_encoding='text',
                 bibtex=None):
        self._id = pub_id
        self.title = title
        self.authors = authors or []
        self.description = description
        self.description_encoding = description_encoding
        self.pdf = pdf
        self.image = image
        self.bibtex = None
        self.book = book
        self.year = year
        self.bibtex = bibtex
        self._category = None  # is set on set_category
        self._bibliography = None  # is set by bibliography
        if bibliography:
            bibliography.add_publication(self, safe_id)

    @property
    def bibliography(self):
        return self._bibliography

    @property
    def category(self):
        return self._category

    def set_category(self, cat_id):
        category = self.bibliography.get_category(cat_id)
        self._category = category

    @property
    def id(self):
        return self._id

    @property
    def described(self):
        return bool(self.description or self.html_description)

    def set_id(self, new_id):
        if new_id != self.id:
            if self.bibliography:
                if self.bibliography.has_publication(new_id):
                    raise ValueError('This id is already taken in the '
                                     'bibliography.')
                del self.bibliography._publications[self.id]
                self.bibliography._publications[new_id] = self
            self._id = new_id

    def __repr__(self):
        return "%s(%s)" % (self.__class__, self.title)
