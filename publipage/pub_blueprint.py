from flask.blueprints import Blueprint
from flask import render_template, abort, send_from_directory
from fs_load import load_bibliography
from settings import BIBLIO_PATH
from model import Author
import os

# expand user dir (e.g. ~)
BIBLIO_PATH = os.path.abspath(os.path.expanduser(BIBLIO_PATH))

pub_blueprint = Blueprint('publipage', __name__,
                          static_folder='static',
                          static_url_path='/publipage/static',
                          template_folder='templates')

biblio = load_bibliography(BIBLIO_PATH)


@pub_blueprint.url_value_preprocessor
def pull_pub(endpoint, values):
    """
    When a request contains a publication value
    replace it automatically by the publication from the library or abort
    """
    if values and 'publication' in values:
        pub_id = values['publication']
        if biblio.has_publication(pub_id):
            values['publication'] = biblio.get_publication(pub_id)
        else:
            abort(404)


@pub_blueprint.route('/')
def publications():
    return render_template('publis.html',
                           bibliography=biblio,
                           splitext=os.path.splitext,
                           abbrev=Author.abbrev_name)


@pub_blueprint.route('/papers/<publication>.pdf')
def get_paper(publication):
    if publication.pdf:
        return send_from_directory(BIBLIO_PATH, publication.pdf)
    else:
        abort(404)


@pub_blueprint.route('/bibtex/<publication>.bib')
def get_bibtex(publication):
    if publication.bibtex:
        return send_from_directory(BIBLIO_PATH, publication.bibtex)
    else:
        abort(404)


@pub_blueprint.route('/images/<publication>.<ext>')
def get_image(publication, ext):
    if publication.image:
        return send_from_directory(BIBLIO_PATH, publication.image)
    else:
        abort(404)
