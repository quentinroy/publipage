$(function(){

    // manage publication click events
    $('.publication-div').on('click', function(evt){

        var pubDiv    = $(this),
            parentA   = $(evt.target).parents('a');

        // do not do anything when the click occurs on a link
        if(parentA.length) return;

        // toggle described div
        if(pubDiv.hasClass('described')) {
            toggleDescription(pubDiv)
        } else {
            bumpDiv(pubDiv);
        }
        evt.preventDefault();
    });

    // toggle the description of a publication div
    function toggleDescription(publicationDiv){

        // ensure the publication div is a jquery object
        publicationDiv = $(publicationDiv);
        var shown = publicationDiv.data('shown') || false;

        if(shown) {
            hideDescr(publicationDiv);
        } else {
           showDescr(publicationDiv);
        } 
        // invert the shown data
        publicationDiv.data('shown', !shown);
    }

    function hideDescr(publicationDiv) {
        var pubDescr = $(publicationDiv).find('.pub-descr');
        // immediately stop any previous animation
        pubDescr.stop(true);
        // hide the description
        pubDescr.hide(100);
    }

    function showDescr(publicationDiv){
        publicationDiv = $(publicationDiv);
        var pubDescr  = publicationDiv.find('.pub-descr'),
            $window   = $(window),
            pubDivTop = publicationDiv.offset().top,
            winHeight = $window.outerHeight();

        // immediately stop any previous animation
        pubDescr.stop(true);

        // check if the bottom of the div is visible and instead, scroll
        // this is executed on each animation frame
        function checkScroll() {
            var pubBottom = pubDivTop + publicationDiv.outerHeight()
                winBottom = winHeight + $window.scrollTop();
            if(pubBottom > winBottom) {
                $window.scrollTop(pubBottom - winHeight);
            }
        }

        // show the descr
        pubDescr.show({
            duration:100,
            progress: checkScroll,
            done: function(){
                checkScroll();
                // this may be required in some browsers (e.g. safari) so that the
                // div is fully visible at the end
                setTimeout(checkScroll, 0);
            }
        });
    }

    // bump the height of a div to show that there is no description
    function bumpDiv(publicationDiv){
        // ensure the publication div is a jquery object
        publicationDiv = $(publicationDiv);
        // register the height for the case where the div is already bumping
        var pubHeight   = publicationDiv.height();
        var baseHeight  = publicationDiv.data('baseHeight') || publicationDiv.height();
        publicationDiv.stop(true);
        publicationDiv.data('baseHeight', baseHeight);
        publicationDiv.animate({
            height: baseHeight + 20
        }, 100).animate({
            height: baseHeight
        }, {
            duration: 100,
            complete: function(){
                console.log('done');
                publicationDiv.css('height', '');
                publicationDiv.removeData('baseHeight');
            }
        });
    }

});