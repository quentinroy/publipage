# -*- coding: utf-8 -*-
"""
Bibtex loading
"""
#!/usr/bin/python

import json
import getopt
from bibtexparser.bparser import BibTexParser
from bibtexparser import customization


def _parser_customisation(record):
    record = customization.convert_to_unicode(record)
    record = customization.type(record)
    record = customization.author(record)
    record = customization.editor(record)
    record = customization.journal(record)
    record = customization.keyword(record)
    record = customization.link(record)
    record = customization.page_double_hyphen(record)
    record = customization.doi(record)

    if 'author' in record:
        record['authors'] = record['author']
        del record['author']
    return record


_parser = BibTexParser()
_parser.customization = _parser_customisation
_parser.homogenise_fields = True


def load(path):
    with open(path, 'r') as bibfile:
        data = _parser.parse_file(bibfile)
        return data.get_entry_list()


def convert(target_path, compact=False):
    return json.dumps(
        load(target_path),
        indent=2 if not compact else None, ensure_ascii=False
    ).encode('utf8')


def _print_usage():
    print 'test.py [-c] <inputpath> [-o <outputfile>]'


if __name__ == '__main__':
    import sys

    argv = sys.argv[1:]
    input_path = None
    output_path = None
    compact = False
    try:
        opts, args = getopt.gnu_getopt(argv, "hi:o:c", ["ifile=", "ofile="])
    except getopt.GetoptError:
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            _print_usage()
            sys.exit()
        elif opt in ("-i", "--ifile"):
            input_path = arg
        elif opt in ("-o", "--ofile"):
            output_path = arg
        elif opt == "-c":
            compact = True
            print('compact')
    if not input_path and args:
        input_path = args[0]

    if not input_path:
        print 'Error: No input file specified'
        _print_usage()
        sys.exit(1)

    json_str = convert(input_path, compact=compact)
    if output_path:
        with open(output_path, 'w') as output_file:
            output_file.write(json_str)
    else:
        print(json_str)
