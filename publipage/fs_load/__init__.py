# -*- coding: utf-8 -*-
"""
Bibliography Management module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This module is in charge of the bilbiography management and in particular
its loading.
"""

import parsers
from load import register_file_type, register_parser

# update bibload property loaders
register_parser(parsers.load_bibtex)
register_parser(parsers.load_json)
register_parser(parsers.load_rst)


# update bibload file types
register_file_type({
    'pdf': 'pdf',
    'image': ('png', 'jpeg', 'jpg'),
    'bibtex': 'bib'
    })

from bib import load_bibliography
