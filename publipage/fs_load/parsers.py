# -*- coding: utf-8 -*-
import bibtex
import json
import os
from docutils.core import publish_parts


def check_ext(path, required_ext, case_sensitive=False):
    "Check the extension of a paths"
    split = os.path.splitext(path)
    if len(split) < 2:
        return required_ext is None or required_ext == ''
    # deal with case sensitivity
    required_ext = required_ext if case_sensitive else required_ext.lower()
    ext = split[1][1:] if case_sensitive else split[1][1:].lower()
    return required_ext == ext


class LoadException(Exception):
    """
    Exception raised when a file should be loaded but cannot for some reasons.
    """
    pass


def _rename_field(values, old_key, new_key):
    "rename a fiel from a dict"
    if old_key in values:
        values[new_key] = values.pop(old_key)
    return values


def load_json(json_file):
    """
    json loader.
    """
    if not check_ext(json_file, 'json'):
        raise LoadException()
    with open(json_file, 'r') as json_data:
        return json.load(json_data)


def load_bibtex(bib_file):
    if not check_ext(bib_file, 'bib'):
        raise LoadException()
    bibtex_values = bibtex.load(bib_file)
    # check if there is only one record
    if len(bibtex_values) != 1:
        raise LoadException(
            'Bibtex config file should contain one and only one entry: {}'
            .format(bib_file))
    entry = bibtex_values[0]
    _rename_field(entry, 'note', 'description')
    return entry


def load_rst(rst_file_path):
    "load a rst file"
    if check_ext(rst_file_path, 'rst'):
        with open(rst_file_path, 'r') as rst_file:
            content = rst_file.read()
            parts = publish_parts(content, writer_name='html4css1')
            return {
                'description': parts['body'],
                'description_encoding': 'html_from_rst'
            }
    else:
        raise LoadException()
