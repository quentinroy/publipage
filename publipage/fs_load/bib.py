import os
import unicodedata
from load import load_bib_records
from ..model import Bibliography, Category, Publication
from markupsafe import escape_silent


DESCRIPTION_FIELDS = ('description',
                      'descr',
                      'note',
                      'notes',
                      'annote')

BOOK_FIELDS = ('booktitle',)


def load_bibliography(path):
    head = load_bib_records(path)
    name = head.attributes.get('name', None)
    description = head.attributes.get('description', None)
    description = description or head.attributes.get('descr', None)
    bib = Bibliography(name=name, description=description, path=path)
    _load_records(head, bib)
    return bib


def _define_record_type(record):
    return 'publication' if len(record) == 0 else 'category'


def _one_of(map, keys):
    for key in keys:
        val = map.get(key, None)
        if val:
            return val


def _load_records(records, bib, parent_cat=None):
    for child in records:
        child_type = _define_record_type(child)
        if child_type == 'publication':
            pub = _load_publication(child, bib)
            if parent_cat:
                pub.set_category(parent_cat.id)
        else:
            cat = _load_category(child, bib)
            if parent_cat:
                cat.set_category(parent_cat.id)


def _load_category(record, bib):
    name = record.attributes.get('name', None)
    # make sure the id is url safe
    cat_id = _make_id(
        record.attributes.get('id', os.path.basename(record.path)))
    description = _one_of(record.attributes, DESCRIPTION_FIELDS)
    category = Category(cat_id,
                        name=name,
                        bibliography=bib,
                        description=description,
                        safe_id=True)
    _load_records(record, bib, category)
    return category


def _plain2html(string):
    string = escape_silent(string)
    if not string:
        return None
    else:
        return str(string).replace('\n', '<br>\n')


def _load_publication(record, bibliography):
    authors_register = bibliography.authors_register
    authors = _one_of(record.attributes, ('author', 'authors'))
    # make sure the id is url safe
    pub_id = _make_id(
        record.attributes.get('id', os.path.basename(record.path)))
    title = record.attributes['title']
    description = record.attributes.get('description', None)
    description_encoding = record.attributes.get('description_encoding', None)
    # convert description to html_description to avoid space and new lines
    # to be removed
    if not description:
        description_text = _one_of(record.attributes, DESCRIPTION_FIELDS)
        description = _plain2html(description_text)
        description_encoding = 'html'

    book = _one_of(record.attributes, BOOK_FIELDS)
    bib_authors = [authors_register.get(author) or authors_register.add(author)
                   for author in authors]

    pub = Publication(pub_id,
                      authors=bib_authors,
                      title=title,
                      bibliography=bibliography,
                      year=record.attributes.get('year', None),
                      safe_id=True,
                      book=book,
                      description=description,
                      description_encoding=description_encoding)
    if 'pdf' in record.files:
        pub.pdf = record.files['pdf'][0]
    if 'image' in record.files:
        pub.image = record.files['image'][0]
    if 'bibtex' in record.files:
        pub.bibtex = record.files['bibtex'][0]
    return pub


# Characters that are safe in any part of an URL.
_safe_cap = b'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
_safe_low = b'0123456789abcdefghijklmnopqrstuvwxyz_'


def _make_id(string):
    # transform all accent
    data = unicodedata.normalize('NFKD', unicode(string))
    ascii_id = data.encode('ASCII', 'ignore')
    # make the id url clean
    final_id = ''
    for character in ascii_id:
        if character in _safe_low:
            final_id += character
        elif character in _safe_cap:
            final_id += character.lower()
        else:
            final_id += '_'
    return final_id
