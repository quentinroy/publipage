import os
import six
from parsers import LoadException, check_ext
from collections import defaultdict


# registers the loaders
attribute_parsers = []

file_types = {}


class FSRecord(object):

    def __init__(self, path):
        super(FSRecord, self).__init__()
        self.attributes = {}
        self.files = defaultdict(list)
        self._parent = None
        self._path = path
        self._records = []

    @property
    def path(self):
        return self._path

    @property
    def parent(self):
        return self._parent

    def add_file(self, file_type, file_path=None):
        self.files[file_type].append(file_path)

    def add_child(self, record):
        self._records.append(record)
        record._parent = self

    def is_empty(self):
        return not 'name' in self.properties and len(self._records) < 1

    def get_child(self, num):
        return self[num]

    def get_base(self):
        if not self.parent:
            return self
        else:
            return self.parent.get_base()

    def __iter__(self):
        return (record for record in self._records)

    def __len__(self):
        return len(self._records)

    def __getitem__(self, num):
        return self._records[num]

    @property
    def is_head(self):
        return not self.parent


def register_file_type(file_type, ext_or_checker=None):
    # case file type is a dict
    if not isinstance(file_type, six.string_types):
        if ext_or_checker or not hasattr(file_type, '__getitem__'):
            raise ValueError()
        for ft, eoc in file_type.iteritems():
            register_file_type(ft, eoc)

    elif hasattr(ext_or_checker, '__iter__'):
        for item in ext_or_checker:
            register_file_type(file_type, item)

    else:
        if file_type not in file_types:
            file_types[file_type] = []
        file_types[file_type].append(ext_or_checker)


def register_parser(loader):
    attribute_parsers.append(loader)


def _check_file_type(file_name, checker):
    if isinstance(checker, six.string_types):
        return check_ext(file_name, checker)
    else:
        return checker(file_name)


def _determine_file_type(file_name):
    for file_type, checkers in file_types.iteritems():
        for checker in checkers:
            if _check_file_type(file_name, checker):
                return file_type


def _load_record(path, max_depth):
    "Load a record"
    record = FSRecord(path)

    record.attributes['name'] = os.path.basename(os.path.abspath(path))

    for sub_path in os.listdir(path or '.'):
        full_path = os.path.join(path, sub_path)
        # check if it is a directory
        if os.path.isdir(full_path):
            if max_depth is None or max_depth >= 0:
                next_max = None if max_depth is None else max_depth - 1
                sub_record = _load_record(full_path, next_max)
                record.add_child(sub_record)
        else:
            # try to load properties from the file
            for attr_loader in attribute_parsers:
                try:
                    attributes = attr_loader(full_path)
                    record.attributes.update(attributes)
                    break
                except LoadException:
                    pass
            # try to determine the type of the file
            file_type = _determine_file_type(full_path)
            if file_type:
                record.add_file(file_type, full_path)

    return record


def load_bib_records(path, max_depth=None):
    curdir = os.path.abspath(os.curdir)
    os.chdir(path)
    bib = _load_record('', max_depth)
    # restore previous directory
    os.chdir(curdir)
    return bib
