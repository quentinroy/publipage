from flask_frozen import Freezer
from serve import app

freezer = Freezer(app)

if __name__ == '__main__':
    freezer.freeze()
